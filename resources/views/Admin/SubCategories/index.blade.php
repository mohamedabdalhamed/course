@extends('Admin.Layout.Layout')
@section('content')

   
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
      <a href="{{url('CreateSubCategories')}}">   <h6 class="text-capitalize ps-3">Add SubCategories</h6></a>

          <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
              <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3" id="ChnageColor">
                <h6 class="text-white text-capitalize ps-3">SubCategories</h6>
              </div>
            </div>
            <div class="card-body px-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                  <thead>
                    <tr>
                      <th class=" text-secondary  font-weight-bolder opacity-7">image</th>
                      <th class="text-center  text-secondary  font-weight-bolder opacity-7">description</th>
                      <th class="text-center  text-secondary  font-weight-bolder opacity-7">date</th>

                      <th class="text-secondary opacity-7">Control</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($categories as $categorie)
                    <tr>
                      <td>
                        <div class="d-flex px-2 py-1">
                          <div>
                            <img src="{{asset('/images/' . $categorie->image)}}" class="avatar avatar-sm me-3 border-radius-lg" alt="user1">
                          </div>
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm">{{ $categorie->name}}</h6>
                          </div>
                        </div>
                      </td>
                 
                      
                      <td class="align-middle text-center">
                      <div class="body">
  <p class="moreRead">{{ substr($categorie->description, 0,  50) }}</p>
<details>
 <summary>
   <span id="open">read more</span> 
   <span id="close">close</span> 
 </summary>
  <p class="lead">
  {{$categorie->description }}
   </p>
</details>
</div>
                        <!-- {{$categorie->description}} -->
                      </td>
                      <td class="align-middle text-center">

                        {{$categorie->created_at}}
                        </td>
                      <td class="align-middle">
                        <a  href="{{url('EditSubCategories',$categorie->id)}}" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Edit user">
                          Edit
                        </a>
                        <a href="{{url('DeleteSubcategories',$categorie->id)}}" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Edit user">
                          Delete
                        </a>
                      </td>
                    </tr>
                   @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            {{$categories->links()}}

          </div>
        </div>
      </div>

      </div>
      </div>
      <script>
      let bggradient= localStorage.getItem('bg-gradient-')
  const ChnageColor = document.getElementById('ChnageColor');

  ChnageColor.classList.add('bg-gradient-' + bggradient);
 


    </script>

@endsection