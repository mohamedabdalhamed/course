@extends('Admin.Layout.Layout')
@section('content')

<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
<form class="m-5" action="{{url('EditcategoriesUpdated',$categories->id)}}" method="post" enctype="multipart/form-data">
    @csrf
  <h1>categories</h1>
  <div class="row mb-4">
    <div class="col col-md-12">
      <div class="form-outline">
      <label class="form-label" for="form6Example1">name</label>
        <input type="text" id="namecategories" class="form-controls" name="name" value="{{$categories->name}}" />
      </div>
    </div>

  </div>
  <script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<div class="file-upload">
  <button class="file-upload-btn " type="button"  id="fileuploadbtn" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>

  <div class="image-upload-wrap " id="imageuploadwrap">
    <input class="file-upload-input" type='file' onchange="readURL(this);" name="image" accept="image/*" />
    <div class="drag-text">
      <h3>Drag and drop a file or select add Image</h3>
    </div>
  </div>
  <div class="file-upload-content">
    <img class="file-upload-image" src="{{asset('/images/' . $categories->image)}}" alt="your image" />
    <div class="image-title-wrap">
      <button type="button" onclick="removeUpload()" class="remove-image bg-gradient-danger">Remove <span class="image-title">Uploaded Image</span></button>
    </div>
  </div>
</div>
  <!-- Message input -->
  <div class="form-outline mb-4">
    <textarea class="form-controls" id="form6Example7" rows="4" name="description">{{$categories->description}}</textarea>
    <label class="form-label" for="form6Example7">description</label>
  </div>
  <!-- Submit button -->
  <button type="submit" class="btn btn-primary btn-block mb-4">Edit</button>
</form>
</main>
<script type="text/javascript">
       function readURL(input) {
  if (input.files && input.files[0]) {

    var reader = new FileReader();

    reader.onload = function(e) {
      $('.image-upload-wrap').hide();

      $('.file-upload-image').attr('src', e.target.result);
      $('.file-upload-content').show();

      $('.image-title').html(input.files[0].name);
    };

    reader.readAsDataURL(input.files[0]);

  } else {
    removeUpload();
  }
}

function removeUpload() {
  $('.file-upload-input').replaceWith($('.file-upload-input').clone());
  $('.file-upload-content').hide();
  $('.image-upload-wrap').show();
}
$('.image-upload-wrap').bind('dragover', function () {
    $('.image-upload-wrap').addClass('image-dropping');
  });
  $('.image-upload-wrap').bind('dragleave', function () {
    $('.image-upload-wrap').removeClass('image-dropping');
});
let bggradient= localStorage.getItem('bg-gradient-')
  const fileuploadbtn = document.getElementById('fileuploadbtn');
  const imageuploadwrap = document.getElementById('imageuploadwrap');

  fileuploadbtn.classList.add('bg-gradient-' + bggradient);
  imageuploadwrap.classList.add('border-' + bggradient);
var parent = document.querySelector(".nav-link.active");

parent.classList.add('bg-gradient-' + bggradient);

$( document ).ready(function() {
    $('.image-upload-wrap').hide();

      $('.file-upload-content').show();

    });
    </script>

@endsection