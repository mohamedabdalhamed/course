$(document).ready(function() {
    const body = document.getElementsByTagName('body')[0];
    const darkversion = document.getElementById('dark-version')
    var sidebar = document.querySelector('.sidenav');

    const hr = document.querySelectorAll('div:not(.sidenav) > hr');
    const hr_card = document.querySelectorAll('div:not(.bg-gradient-dark) hr');
    const text_btn = document.querySelectorAll('button:not(.btn) > .text-dark');
    const text_span = document.querySelectorAll('span.text-dark, .breadcrumb .text-dark');
    const text_span_white = document.querySelectorAll('span.text-white, .breadcrumb .text-white');
    const text_strong = document.querySelectorAll('strong.text-dark');
    const text_strong_white = document.querySelectorAll('strong.text-white');
    const text_nav_link = document.querySelectorAll('a.nav-link.text-dark');
    const text_nav_link_white = document.querySelectorAll('a.nav-link.text-white');
    const secondary = document.querySelectorAll('.text-secondary');
    const bg_gray_100 = document.querySelectorAll('.bg-gray-100');
    const bg_gray_600 = document.querySelectorAll('.bg-gray-600');
    const btn_text_dark = document.querySelectorAll('.btn.btn-link.text-dark, .material-icons.text-dark');
    const btn_text_white = document.querySelectorAll('.btn.btn-link.text-white, .material-icons.text-white');
    const card_border = document.querySelectorAll('.card.border');
    const card_border_dark = document.querySelectorAll('.card.border.border-dark');
    const svg = document.querySelectorAll('g');
          let getBackGroundVersion=    localStorage.getItem('body')
        if(getBackGroundVersion){
  
          body.classList.add('dark-version');
  
      for (var i = 0; i < hr.length; i++) {
        if (hr[i].classList.contains('dark')) {
          hr[i].classList.remove('dark');
          hr[i].classList.add('light');
        }
      }
  
      for (var i = 0; i < hr_card.length; i++) {
        if (hr_card[i].classList.contains('dark')) {
          hr_card[i].classList.remove('dark');
          hr_card[i].classList.add('light');
        }
      }
      for (var i = 0; i < text_btn.length; i++) {
        if (text_btn[i].classList.contains('text-dark')) {
          text_btn[i].classList.remove('text-dark');
          text_btn[i].classList.add('text-white');
        }
      }
      for (var i = 0; i < text_span.length; i++) {
        if (text_span[i].classList.contains('text-dark')) {
          text_span[i].classList.remove('text-dark');
          text_span[i].classList.add('text-white');
        }
      }
      for (var i = 0; i < text_strong.length; i++) {
        if (text_strong[i].classList.contains('text-dark')) {
          text_strong[i].classList.remove('text-dark');
          text_strong[i].classList.add('text-white');
        }
      }
      for (var i = 0; i < text_nav_link.length; i++) {
        if (text_nav_link[i].classList.contains('text-dark')) {
          text_nav_link[i].classList.remove('text-dark');
          text_nav_link[i].classList.add('text-white');
        }
      }
      for (var i = 0; i < secondary.length; i++) {
        if (secondary[i].classList.contains('text-secondary')) {
          secondary[i].classList.remove('text-secondary');
          secondary[i].classList.add('text-white');
          secondary[i].classList.add('opacity-8');
        }
      }
      for (var i = 0; i < bg_gray_100.length; i++) {
        if (bg_gray_100[i].classList.contains('bg-gray-100')) {
          bg_gray_100[i].classList.remove('bg-gray-100');
          bg_gray_100[i].classList.add('bg-gray-600');
        }
      }
      for (var i = 0; i < btn_text_dark.length; i++) {
        btn_text_dark[i].classList.remove('text-dark');
        btn_text_dark[i].classList.add('text-white');
      }
      for (var i = 0; i < svg.length; i++) {
        if (svg[i].hasAttribute('fill')) {
          svg[i].setAttribute('fill', '#fff');
        }
      }
      for (var i = 0; i < card_border.length; i++) {
        card_border[i].classList.add('border-dark');
      }
      darkversion.setAttribute("checked", "true");
        }

        let classes = ['position-sticky', 'blur', 'shadow-blur', 'mt-4', 'left-auto', 'top-1', 'z-index-sticky'];
  const navbar = document.getElementById('navbarBlur');
  const navbarFixed = document.getElementById('navbarFixed');

 let getNavbar= localStorage.getItem('navbar')
 
  if (getNavbar) {
    navbar.classList.add(...classes);
    navbar.setAttribute('navbar-scroll', 'true');
    navbarBlurOnScroll('navbarBlur');
    navbarFixed.setAttribute("checked", "true");
  }else{
    navbarFixed.removeAttribute("checked");

  }
  var parent = document.querySelector(".nav-link.active");

  let bggradient= localStorage.getItem('bg-gradient-')
  parent.classList.add('bg-gradient-' + bggradient);

  let sidebarTypecolor= localStorage.getItem('sidebarTypecolor')
  let sidebarTypetext= localStorage.getItem('sidebarTypetext')

  if (sidebarTypecolor == 'bg-transparent' || sidebarTypecolor == 'bg-white') {
    var textWhites = document.querySelectorAll('.sidenav .text-white');
    for (let i = 0; i < textWhites.length; i++) {
        textWhites[i].classList.remove('text-white');

      textWhites[i].classList.add('text-dark');
    }

  } else {
    var textDarks = document.querySelectorAll('.sidenav .text-dark');
    for (let i = 0; i < textDarks.length; i++) {
      textDarks[i].classList.add('text-white');
      textDarks[i].classList.remove('text-dark');
    }

  }
  const sidebarTypecolors = document.getElementById(sidebarTypecolor);
  var parent =sidebarTypecolors.parentElement.children;

  for (var i = 0; i < parent.length; i++) {
    parent[i].classList.remove('active');
  }
  sidebarTypecolors.classList.add('active');

  sidebar.classList.add(sidebarTypecolor);
  const ChnageColor = document.getElementById('ChnageColor');

  ChnageColor.classList.add('bg-gradient-' + bggradient);
 
          });