<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\categories;

class categoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function Categories()
    {
        $categories=categories::orderBy('id')->paginate(9);
        return view('Admin.Course.index',compact('categories'));
    }
    
    public function CreateCategories()
    {
        return view('Admin.Course.create');
    }
    public function Addcategories(Request $Request)
    {    
        $vaild= $Request->validate([
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);

        if($vaild){
            $categories=new categories();
            $categories->name=$Request->name;
            $imageName = time().'.'.$Request->image->extension();  
            $Request->image->move(public_path('images'), $imageName);
            $categories->image= $imageName;
            $categories->description=$Request->description;
            $categories->save();

            return back()->with('success','Item created successfully!');
        }else{

            return back()->with('warning','This is not images!');
        }
        }
        public function Editcategories($id)
        {
            $categories=categories::find($id);
            return view('Admin.Course.Edit',compact('categories'));
        }
        public function Updated($id,Request $Request)
        {
            $categories=categories::find($id);
            $categories->name=$Request->name;
            $categories->description=$Request->description; 
                if($Request->image!==null){
                    $imageName = time().'.'.$Request->image->extension(); 
                    $Request->image->move(public_path('images'), $imageName);
                    $categories->image= $imageName;
                    
                }
                $categories->save();
                return back()->with('success','Item created successfully!');
        }
        public function Delete($id)
        {
            $categories=categories::where('id',$id)->delete();
            return back()->with('success','Item Delete successfully!');
        }


}
