<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubCategories;
use App\categories;

class SubcategoriesController extends Controller
{
    public function SubCategories()
    {
        $categories=SubCategories::orderBy('id')->paginate(9);
        return view('Admin.SubCategories.index',compact('categories'));
    }
    
    public function CreateSubCategories()
    {
        $categories=categories::all();
        return view('Admin.SubCategories.create',compact('categories'));
    }
    public function AddSubCategories(Request $Request)
    {    
           $SubCategories=new SubCategories();
            $SubCategories->name=$Request->name;
            $SubCategories->categorie_id=$Request->categories_id;
            $imageName = time().'.'.$Request->image->extension();  
            $Request->image->move(public_path('images'), $imageName);
            $SubCategories->image= $imageName;
            $SubCategories->description=$Request->description;
            $SubCategories->save();

            return back()->with('success','Item created successfully!');
       
        }
        public function EditSubCategories($id)
        {
            $SubCategories=SubCategories::find($id);
            $categoriesAll=categories::all();

            
            return view('Admin.SubCategories.Edit',compact('SubCategories','categoriesAll'));
        }
        public function Updated($id,Request $Request)
        {
            $SubCategories=SubCategories::find($id);
            $SubCategories->name=$Request->name;
            $SubCategories->description=$Request->description; 
            $SubCategories->categorie_id=$Request->categories_id;
                if($Request->image!==null){
                    $imageName = time().'.'.$Request->image->extension(); 
                    $Request->image->move(public_path('images'), $imageName);
                    $SubCategories->image= $imageName;
                    
                }
                $SubCategories->save();
                return back()->with('success','Item created successfully!');
        }
        public function Delete($id)
        {
            $SubCategories=SubCategories::where('id',$id)->delete();
            return back()->with('success','Item Delete successfully!');
        }

}
