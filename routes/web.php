<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/Permssion', 'accessController@Permssion')->name('Permssion');

Route::middleware(['admin'])->group(function () {
    Route::get('/Admin', 'HomeController@Admin')->name('Admin');
    // Categories
    Route::get('/Categories', 'categoriesController@Categories')->name('Categories');
    Route::get('/CreateCategories', 'categoriesController@CreateCategories')->name('CreateCategories');
    Route::post('/Addcategories','categoriesController@Addcategories');
    Route::get('/Editcategories/{id}','categoriesController@Editcategories');
    Route::post('/EditcategoriesUpdated/{id}','categoriesController@Updated');
    Route::get('/Deletecategories/{id}','categoriesController@Delete');

    // End Categories

      // SubCategories
      Route::get('/SubCategories', 'SubcategoriesController@SubCategories')->name('SubCategories');
      Route::get('/CreateSubCategories', 'SubcategoriesController@CreateSubCategories')->name('CreateSubCategories');
      Route::post('/AddSubCategories','SubcategoriesController@AddSubCategories');
      Route::get('/EditSubCategories/{id}','SubcategoriesController@EditSubCategories');
      Route::post('/EditSubcategoriesUpdated/{id}','SubcategoriesController@Updated');
      Route::get('/DeleteSubcategories/{id}','SubcategoriesController@Delete');
  
      // End SubCategories
});
Auth::routes();
